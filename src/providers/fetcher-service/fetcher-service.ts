import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class FetcherServiceProvider {
    data: any;
    // static flag: string = "p";
    // private baseUrl: string = environment.firebase.baseUrl;
    constructor(public http: Http) {
        // console.log('Hello FetcherServiceProvider Provider');
    }
    sendNotification(body) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return new Promise(resolve => {
            let url = 'https://onesignal.com/api/v1/notifications'
            this.http.post(url, body, { headers: headers })
                .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                });
        });
    }
}