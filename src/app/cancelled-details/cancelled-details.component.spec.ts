import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelledDetailsComponent } from './cancelled-details.component';

describe('CancelledDetailsComponent', () => {
  let component: CancelledDetailsComponent;
  let fixture: ComponentFixture<CancelledDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelledDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelledDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
