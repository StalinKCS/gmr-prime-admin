import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { NgxSpinnerService } from 'ngx-spinner';
declare var bootbox: any;
@Component({
  selector: 'app-corporate',
  templateUrl: './corporate.component.html',
  styleUrls: ['./corporate.component.css']
})
export class CorporateComponent implements OnInit {
  mobileNo: any;
  email: any;
  helpDisk: any;
  constructor(
    private afDB: AngularFireDatabase,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.loadHelpDisk();
  }

  loadHelpDisk() {
    this.spinner.show();
    this.afDB.object("/helpdesk/").valueChanges().subscribe(res => {
      this.spinner.hide();
      this.helpDisk = res;
      this.mobileNo = res["mobile"]
      this.email = res["email"]
     //console.log(this.helpDisk);
    })
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  isEmpty(val) {
    return (val === false || val === '' || val === undefined || val == null || val.length <= 0) ? true : false;
  }
  _toUpdate()
  {
    var vm = this;
    var regEmail = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    if (vm.isEmpty(vm.email)) {
      bootbox.alert('Please enter a email-ID!');
    } else if (!regEmail.test(vm.email)) {
      bootbox.alert('Enter a valid Email ID.')
    } else if (vm.isEmpty(vm.mobileNo)) {
      bootbox.alert('Please enter your mobile number!!');
    } else if (vm.mobileNo.toString().length != 10) {
      bootbox.alert('Please enter a valid mobile number!');
    } else {
      let payload = {
        "email":vm.email,
        "mobile":vm.mobileNo
      }
      vm.afDB.object('helpdesk/').update(payload);
      bootbox.alert("Updated successfully");
    }

  }

}
