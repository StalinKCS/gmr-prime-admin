import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { AngularFireDatabase } from 'angularfire2/database';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import * as FileSaver from 'file-saver';
// import * as moment from 'moment';
declare var bootbox: any;
import * as moment from 'moment-timezone';
@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.css']
})
export class ManageUsersComponent implements OnInit {
  dataTable_1: any;
  dataTable_2: any;
  dataTable_3: any;

  staffsPath: any;
  adminObj: any;
  grlObj: any;
  groObj: any;
  constructor(private spinner: NgxSpinnerService, public db: AngularFireDatabase, public chRef: ChangeDetectorRef) { }

  // ngAfterViewInit() {
  //   this.getUser();
  // }

  ngOnInit() {
    this.getStaffs();
  }

  // getDate(timeStamp) {
  //   var date = moment(timeStamp).format("DD-MM-YYYY")
  //   return date;
  // }

  getStaffs() {
    this.spinner.show();
    this.staffsPath = this.db.object('staffDetails/').valueChanges();
    this.staffsPath.subscribe(res => {
      this.spinner.hide();
      this.adminObj = Object.entries(res["admin"]).map(([objId, value]) => ({ objId, value }));
      this.grlObj = Object.entries(res["grl"]).map(([objId, value]) => ({ objId, value }));
      this.groObj = Object.entries(res["gro"]).map(([objId, value]) => ({ objId, value }));
      this.chRef.detectChanges();
      const table_1: any = $('#table_1');
      this.dataTable_1 = table_1.DataTable();
      const table_2: any = $('#table_2');
      this.dataTable_2 = table_2.DataTable();
      const table_3: any = $('#table_3');
      this.dataTable_3 = table_3.DataTable();
    })
  }
  getDate(timeStamp)
  {
    var formatDate;
    var date = new Date(timeStamp).toISOString();
    formatDate = date.substring(0, 4) + "-" + date.substring(4, 6) + "-" + date.substring(6, 8) + "T" + date.substring(8, 10) + ":" + date.substring(10, 12) + ":" + date.substring(12, 14) + ".000Z";
    formatDate =  moment.utc(date).format("MMM DD YYYY [at] HH:mm A");
    return formatDate;
  }

  exportToExcelAdmin(e) {
    var blob = new Blob([document.getElementById('adminDetails').outerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    FileSaver.saveAs(blob, 'Admin-' + new Date().getTime());
  };
  exportToExcelGrl(e) {
    var blob = new Blob([document.getElementById('grlDetails').outerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    FileSaver.saveAs(blob, 'GRL-' + new Date().getTime());
  };
  exportToExcelGro(e) {
    var blob = new Blob([document.getElementById('groDetails').outerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    FileSaver.saveAs(blob, 'GRO-' + new Date().getTime());
  };

  disableStaff(result){
    var vm = this;
    bootbox.confirm("Are you sure to disable the staff " + result.name + "?",
      function (res) {
        if(res){
            let payLoad = {
              isEnable: false
            }
          
        vm.db.object('staffDetails/' + result.role + '/' + result.uid).update(payLoad);
      }
    });
  }

}
