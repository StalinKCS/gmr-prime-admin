import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { AngularFireDatabase } from 'angularfire2/database';
import { HttpModule } from '@angular/http';
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ManageBookingsComponent } from './manage-bookings/manage-bookings.component';
import { PackagDetailsComponent } from './packag-details/packag-details.component';
import { ReportsComponent } from './reports/reports.component';
import { HelpCenterComponent } from './help-center/help-center.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AddUserComponent } from './add-user/add-user.component';
import { FormsModule } from '@angular/forms';
import { ManageRosterComponent } from './manage-roster/manage-roster.component';

import { CalendarModule } from 'angular-calendar';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { NgDatepickerModule } from '../ng-datepicker/module/ng-datepicker.module';
import { WiFiComponent } from './wi-fi/wi-fi.component';
import { TermsAndConditionComponent } from './terms-and-condition/terms-and-condition.component';
import { AuthGuardService as AuthGuard } from './auth/auth-guard.service';
import { PaymentsComponent } from './payments/payments.component';
import { GuestInfoComponent } from './guest-info/guest-info.component';
import { EnquiryDetailsComponent } from './enquiry-details/enquiry-details.component';
import { FetcherServiceProvider } from '../providers/fetcher-service/fetcher-service';
import { CorporateComponent } from './corporate/corporate.component';
import { TdyScheduledComponent } from './tdy-scheduled/tdy-scheduled.component';
import { BookedDetailsComponent } from './booked-details/booked-details.component';
import { CompletedDetailsComponent } from './completed-details/completed-details.component';
import { CancelledDetailsComponent } from './cancelled-details/cancelled-details.component';
import { StaffDetailsComponent } from './staff-details/staff-details.component';
import { GroLoggedDetailsComponent } from './gro-logged-details/gro-logged-details.component';
import { BookingComponent } from './booking/booking.component';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'manage-bookings', component: ManageBookingsComponent, canActivate: [AuthGuard] },
  { path: 'manage-users', component: ManageUsersComponent, canActivate: [AuthGuard] },
  { path: 'package-details', component: PackagDetailsComponent, canActivate: [AuthGuard] },
  { path: 'reports', component: ReportsComponent, canActivate: [AuthGuard] },
  { path: 'help-center', component: HelpCenterComponent, canActivate: [AuthGuard] },
  { path: 'add-user', component: AddUserComponent, canActivate: [AuthGuard] },
  { path: 'manage-roster', component: ManageRosterComponent, canActivate: [AuthGuard] },
  { path: 'wi-fi', component: WiFiComponent, canActivate: [AuthGuard] },
  { path: 'terms-and-condition', component: TermsAndConditionComponent, canActivate: [AuthGuard] },
  { path: 'payments', component: PaymentsComponent, canActivate: [AuthGuard] },
  { path: 'guest-info', component: GuestInfoComponent, canActivate: [AuthGuard] },
  { path: 'enquiry-details', component: EnquiryDetailsComponent, canActivate: [AuthGuard] },
  { path: 'corporate', component: CorporateComponent, canActivate: [AuthGuard] },
  { path: 'tdy-scheduled', component: TdyScheduledComponent, canActivate: [AuthGuard] },
  { path: 'booked-details', component: BookedDetailsComponent, canActivate: [AuthGuard] },
  { path: 'cancelled-details', component: CancelledDetailsComponent, canActivate: [AuthGuard] },
  { path: 'completed-details', component: CompletedDetailsComponent, canActivate: [AuthGuard] },
  { path: 'staff-details', component: StaffDetailsComponent, canActivate: [AuthGuard] },
  { path: 'gro-logged-details', component: GroLoggedDetailsComponent, canActivate: [AuthGuard] },
  { path: 'booking', component: BookingComponent, canActivate: [AuthGuard] }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ManageUsersComponent,
    SidebarComponent,
    DashboardComponent,
    ManageBookingsComponent,
    PackagDetailsComponent,
    ReportsComponent,
    HelpCenterComponent,
    SpinnerComponent,
    AddUserComponent,
    ManageRosterComponent,
    WiFiComponent,
    TermsAndConditionComponent,
    PaymentsComponent,
    GuestInfoComponent,
    EnquiryDetailsComponent,
    CorporateComponent,
    TdyScheduledComponent,
    BookedDetailsComponent,
    CompletedDetailsComponent,
    CancelledDetailsComponent,
    StaffDetailsComponent,
    GroLoggedDetailsComponent,
    BookingComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    NgxSpinnerModule,
    FormsModule,
    CalendarModule,
    NgbDatepickerModule.forRoot(),
    NgDatepickerModule,
    AngularFireModule.initializeApp(environment.firebase),
    RouterModule.forRoot(
      appRoutes,
      { useHash: false } // <-- debugging purposes only
    )
  ],
  providers: [AngularFireAuth, AngularFireDatabase, AuthGuard, FetcherServiceProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }