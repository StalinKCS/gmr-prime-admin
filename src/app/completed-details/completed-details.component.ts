import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { NgxSpinnerService } from 'ngx-spinner';
// import * as moment from 'moment';
declare var bootbox: any;
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FetcherServiceProvider } from '../../providers/fetcher-service/fetcher-service';
import { environment } from '../../environments/environment';
import * as moment from 'moment-timezone';
import * as firebase from 'firebase';
declare var jQuery: any;
import DataTable from 'datatables.net';
import 'datatables.net-bs4';
import { AngularFireAuth } from 'angularfire2/auth';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-completed-details',
  templateUrl: './completed-details.component.html',
  styleUrls: ['./completed-details.component.css']
})
export class CompletedDetailsComponent implements OnInit {
  @ViewChild('closeBtn') closeBtn: ElementRef;
  @ViewChild('openReScheduleModel') openReScheduleModel: ElementRef;
  dataTable_1: any;
  dataTable_2: any;
  dataTable_3: any;
  bookingPath: any;
  arrivalObj: any;
  departureObj: any;
  transferObj: any;
  bookedId: any;
  bookedInfo: any;
  staffsPath: any;
  groObj: any;
  othersGro: any;
  userPath: any;
  userObj: any;
  selectedGro: any;

  bookedSlot: any;
  slotInfo: any;
  bookedInfo_1: any;
  bookingId: any;
  currentSlot: any;
  uId: any;
  constructor(public afAuth: AngularFireAuth, public fetcher: FetcherServiceProvider, public router: Router, private spinner: NgxSpinnerService, public db: AngularFireDatabase, public chRef: ChangeDetectorRef) {
    this.selectedGro = '';
  }

  ngOnInit() {
    this.bookeDetails();

    this.afAuth.authState.subscribe((loggedInUser) => {
      if (loggedInUser == null) {
      } else {
        this.uId = loggedInUser.uid;
      }
    });

    jQuery("#a_from_date").datepicker({
      dateFormat: 'dd/mm/yy',
      onSelect: function (start_date) {
        jQuery("#a_to_date").datepicker({
          dateFormat: 'dd/mm/yy',
        });
        jQuery("#a_to_date").datepicker("option", "minDate", start_date);
      }
    });

    jQuery("#d_from_date").datepicker({
      dateFormat: 'dd/mm/yy',
      onSelect: function (start_date) {
        jQuery("#d_to_date").datepicker({
          dateFormat: 'dd/mm/yy',
        });
        jQuery("#d_to_date").datepicker("option", "minDate", start_date);
      }
    });

    jQuery("#t_from_date").datepicker({
      dateFormat: 'dd/mm/yy',
      onSelect: function (start_date) {
        jQuery("#t_to_date").datepicker({
          dateFormat: 'dd/mm/yy',
        });
        jQuery("#t_to_date").datepicker("option", "minDate", start_date);
      }
    });
  }

  filterTable(table, from, to, col) {
    if (jQuery(from).val() && jQuery(to).val()) {
      DataTable.ext.search.pop();
      DataTable.ext.search.push(
        function (settings, data, dataIndex) {
          var dateStart = jQuery(from).val();
          var dateEnd = jQuery(to).val();
          var slotDate = data[col].split('|')[0];

          var slot = new Date(moment(new Date(slotDate), 'DD/MM/YYYY').format("LLLL"));
          var start = new Date(moment(dateStart, 'DD/MM/YYYY').format("LLLL"));
          var end = new Date(moment(dateEnd, 'DD/MM/YYYY').format("LLLL"));

          if ((slot >= start) && (slot <= end)) {
            return true;
          }
          return false;
        }
      );

      var table: any = $(table);
      table = table.DataTable();
      table.draw();
    } else {
      bootbox.alert("Please select a valid date.");
    }
  }

  bookeDetails() {
    this.spinner.show();
    this.bookingPath = this.db.object('bookingDetails/').valueChanges();
    this.bookingPath.subscribe(res => {
      console.log(res);
      this.spinner.hide();
      if (res["arrival"]) {
        this.arrivalObj = Object.entries(res["arrival"]).map(([objId, value]) => ({ objId, value }));
        this.arrivalObj =  this.arrivalObj.filter(function(resp) {
          // console.log(resp.value.status);
          return resp.value.status.toLowerCase() == "completed";
        });
        this.arrivalObj.sort(function (a, b) {
          var dateA = moment(a.value.slotInfo.slotTimeLabel1).format("YYYY-MM-DD") + " " + a.value.slotInfo.slotTimeLabel2.toString().split(' ')[0]
          var dateB = moment(b.value.slotInfo.slotTimeLabel1).format("YYYY-MM-DD") + " " + b.value.slotInfo.slotTimeLabel2.toString().split(' ')[0]
          return +new Date(dateB) - +new Date(dateA);
        });
      }
      else {
        this.arrivalObj = [];
      }
      if (res["departure"]) {
        this.departureObj = Object.entries(res["departure"]).map(([objId, value]) => ({ objId, value }));
        this.departureObj =  this.departureObj.filter(function(resp) {
          // console.log(resp.value.status);
          return resp.value.status.toLowerCase() == "completed";
        });
        this.departureObj.sort(function (a, b) {
          var dateA = moment(a.value.slotInfo.slotTimeLabel1).format("YYYY-MM-DD") + " " + a.value.slotInfo.slotTimeLabel2.toString().split(' ')[0]
          var dateB = moment(b.value.slotInfo.slotTimeLabel1).format("YYYY-MM-DD") + " " + b.value.slotInfo.slotTimeLabel2.toString().split(' ')[0]
          return +new Date(dateB) - +new Date(dateA);
        });
      }
      else {
        this.departureObj = [];
      }
      if (res["transfer"]) {
        this.transferObj = Object.entries(res["transfer"]).map(([objId, value]) => ({ objId, value }));
        this.transferObj =  this.transferObj.filter(function(resp) {
          // console.log(resp.value.status);
          return resp.value.status.toLowerCase() == "completed";
        });
        this.transferObj.sort(function (a, b) {
          var dateA = moment(a.value.slotInfo.slotTimeLabel1).format("YYYY-MM-DD") + " " + a.value.slotInfo.slotTimeLabel2.toString().split(' ')[0]
          var dateB = moment(b.value.slotInfo.slotTimeLabel1).format("YYYY-MM-DD") + " " + b.value.slotInfo.slotTimeLabel2.toString().split(' ')[0]
          return +new Date(dateB) - +new Date(dateA);
        });
      }
      else {
        this.transferObj = [];
      }
      //console.log(this.arrivalObj);
      this.chRef.detectChanges();
      const table_1: any = $('#table_1');
      this.dataTable_1 = table_1.DataTable();
      const table_2: any = $('#table_2');
      this.dataTable_2 = table_2.DataTable();
      const table_3: any = $('#table_3');
      this.dataTable_3 = table_3.DataTable();
    })
  }



  // ngOnChanges() {
  //   this.chRef.detectChanges();
  //   const table_1: any = $('#table_1');
  //   this.dataTable_1 = table_1.DataTable();
  //   const table_2: any = $('#table_2');
  //   this.dataTable_2 = table_2.DataTable();
  //   const table_3: any = $('#table_3');
  //   this.dataTable_3 = table_3.DataTable();
  // }

  _bookedInfo(bookedId, info) {
    this.bookedId = '';
    this.bookedInfo = {};
    this.bookedId = bookedId;
    this.bookedInfo = info;
    // console.log(this.bookedInfo);
  }

  getDate(timeStamp) {
    var date = moment(timeStamp).format("DD-MM-YYYY")
    return date;
  }

  reAssign(uid) {
    this.othersGro = [];
    this.spinner.show();
    this.staffsPath = this.db.object('staffDetails/gro').valueChanges();
    this.staffsPath.subscribe(res => {
      this.spinner.hide();
      this.groObj = Object.entries(res).map(([objId, value]) => ({ objId, value }));
      for (let i = 0; i < this.groObj.length; i++) {
        if (this.groObj[i].objId != uid) {
          this.othersGro.push(this.groObj[i]);
        }
      }
      // console.log(this.othersGro);
    })
  }

  isEmpty(val) {
    return (val === false || val === '' || val === undefined || val == null || val.length <= 0) ? true : false;
  }


  reschedule(bookeDetail, sltLabel1, sltLabel2, bookingId) {
    var vm = this;
    vm.bookedSlot = [];
    vm.bookingId = "";
    vm.bookingId = bookingId;
    //console.log(bookeDetail)
    var flInfo = bookeDetail["flInfo"];
    //console.log(flInfo);
    var slotInfo = bookeDetail["slotInfo"];
    var flTime = flInfo["flTimeLabel1"] + " " + flInfo["flTimeLabel2"].replace(" hrs (IST)", "");
    var flTimeTz = moment.tz(flTime, 'ddd, MMM DD YYYY HH:mm', 'Asia/Kolkata');
    var slotTime = slotInfo["slotTimeLabel1"] + " " + slotInfo["slotTimeLabel2"].replace(" hrs (IST)", "");
    var slotTimeTz = moment.tz(slotTime, 'ddd, MMM DD YYYY HH:mm', 'Asia/Kolkata');
    // console.log(flTimeTz);
    // console.log(flTimeTz.format('YYYY-MM-DD HH:mm:ss A'));
    // console.log(slotTimeTz);
    // console.log(slotTimeTz.format('YYYY-MM-DD HH:mm:ss A'));
    if (flInfo["mode"] === "domestic") {
      var slot1From = moment(flTimeTz).subtract(3, 'hours');
      var slot1To = moment(flTimeTz).subtract(2, 'hours');
      var slot1TimeLabel1 = slot1From.format('ddd, MMM DD YYYY HH:mm').substring(0, 16);
      var slot1TimeLabel2 = slot1From.format('ddd, MMM DD YYYY HH:mm').substring(17, 23) + " hrs (IST)";
      var slot2From = slot1To;
      var slot2To = moment(flTimeTz).subtract(1, 'hours');
      var slot2TimeLabel1 = slot2From.format('ddd, MMM DD YYYY HH:mm').substring(0, 16);
      var slot2TimeLabel2 = slot2From.format('ddd, MMM DD YYYY HH:mm').substring(17, 23) + " hrs (IST)";
      var slots = [
        {
          "slotFrom": slot1From.format('YYYY-MM-DD HH:mm:ss A'),
          "slotTo": slot1To.format('YYYY-MM-DD HH:mm:ss A'),
          "slotTimeLabel1": slot1TimeLabel1,
          "slotTimeLabel2": slot1TimeLabel2
        },
        {
          "slotFrom": slot2From.format('YYYY-MM-DD HH:mm:ss A'),
          "slotTo": slot2To.format('YYYY-MM-DD HH:mm:ss A'),
          "slotTimeLabel1": slot2TimeLabel1,
          "slotTimeLabel2": slot2TimeLabel2
        }
      ];
      // console.log(slots);
      var availableSlots = [];
      for (var s in slots) {
        if (slots[s]["slotFrom"] != slotTimeTz.format('YYYY-MM-DD HH:mm:ss A')) {
          availableSlots.push(slots[s]);
          break;
        }
      }
      if (availableSlots.length > 0) {
        // console.log(availableSlots);
        vm.slotInfo = availableSlots[0];
        vm.bookedInfo_1 = bookeDetail;
        vm.currentSlot = {
          "slotTimeLabel1": sltLabel1,
          "slotTimeLabel2": sltLabel2
        }
        var availableSlotsFrom = vm.slotInfo["slotFrom"].split(" ");
        var availableSlotsTo = vm.slotInfo["slotTo"].split(" ");
        var slotObj = {
          "frtime": availableSlotsFrom[1].substring(0, 5),
          "frmeridian": availableSlotsFrom[2],
          "totime": availableSlotsTo[1].substring(0, 5),
          "tomeridian": availableSlotsTo[2]
        }
        vm.bookedSlot.push(slotObj);
        vm.openReScheduleModel.nativeElement.click();
        //$('#reSchedule').modal('show');
      }
    }
  }

  chooseTime() {
    var vm = this;
    bootbox.confirm("Are you sure you want to reshedule?", function (result) {
      if (result) {
        // We need to restrict slot reschedule before XX hours
        var service = vm.bookedInfo_1["service"];
        // console.log(vm.currentSlot);
        var bookingIDPath = "/bookingDetails/" + service + "/" + vm.bookingId;
        var updatedSlot = vm.bookedInfo_1["slotInfo"];
        var slotInfoPath = bookingIDPath + "/slotInfo/";
        updatedSlot["slotTimeLabel1"] = vm.slotInfo["slotTimeLabel1"];
        updatedSlot["slotTimeLabel2"] = vm.slotInfo["slotTimeLabel2"];
        var reschedulePath = bookingIDPath + "/rescheduledInfo";
        var rescheduledInfo = {
          "previousBookedSlot": vm.currentSlot
        };
        var update = {};
        update[slotInfoPath] = updatedSlot;
        // update[slotInfoPath] = {"slotTimeLabel1":this.availableSlots["slotTimeLabel1"], "slotTimeLabel2":this.availableSlots["slotTimeLabel2"]};
        update[reschedulePath] = rescheduledInfo;
        //console.log(update);
        vm.db.object('/').update(update);
        vm.closeModal();
      }
    });
  }

  _toUpdate(service, uId) {
    // console.log(service);
    if (this.isEmpty(uId)) {
      bootbox.alert('Please select a GRO!');
    }
    else {
      var vm = this;
      var selectdGRO = {};
      selectdGRO = this.othersGro.filter(
        res => res.objId === uId);
      bootbox.confirm("Are you sure want to Re assign?",
        function (result) {
          if (result) {
            let updatePath = 'bookingDetails/' + service + '/' + vm.bookedId + '/slotInfo/groInfo';
            //console.log(updatePath);
            var payLoad = {
              "assignDate": new Date().getTime(),
              "email": selectdGRO[0].value.email,
              "mobileNo": selectdGRO[0].value.mobileNo,
              "name": selectdGRO[0].value.name,
              "uid": selectdGRO[0].value.uid
            }
            //console.log(payLoad);
            vm.db.object(updatePath).update(payLoad);
            vm.closeModal();
          }
        });
    }
  }

  private closeModal(): void {
    this.othersGro = [];
    this.selectedGro = '';
    this.closeBtn.nativeElement.click();
    //this.chRef.markForCheck();
  }

  exportToExcelArrival(e) {
    var blob = new Blob([document.getElementById('arrivalDetails').outerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    FileSaver.saveAs(blob, 'Arrival-' + new Date().getTime());
  };
  exportToExcelDeparture(e) {
    var blob = new Blob([document.getElementById('departureDetails').outerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    FileSaver.saveAs(blob, 'Departure-' + new Date().getTime());
  };
  exportToExcelTransfer(e) {
    var blob = new Blob([document.getElementById('transferDetails').outerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    FileSaver.saveAs(blob, 'Transfer-' + new Date().getTime());
  };

  cancelPlan(userId, serviceMode, bookingID) {
    var vm = this;
    bootbox.confirm("Are you sure you want to cancel your " + serviceMode + " booking?",
      function (result) {
        if (result) {
          vm.spinner.show();
          var bookingDetailPath = "/bookingDetails/" + serviceMode + "/" + bookingID + "/status";
          var userBookingPath = "/userDetails/" + userId + "/booking/" + bookingID + "/status";
          var cancelledPath = "/bookingDetails/" + serviceMode + "/" + bookingID + "/cancelledInfo";
          var update = {};
          var cancelledInfo = {
            "cancelledAt": new Date().getTime(),
            "cancelledBy": vm.uId
          };
          update[bookingDetailPath] = "cancelled";
          update[cancelledPath] = cancelledInfo;
          update[userBookingPath] = "cancelled";
          vm.db.object('/').update(update);
          //Send ondSignal Push Notfication
          var ref = firebase.database().ref('userDetails/' + userId);
          ref.orderByValue().on("value", function (data) {
            console.log(data.val()["oneSignal"])
            if (data.val()["oneSignal"]) {
              var payLoad =
              {
                "app_id": environment.firebase.app_id,
                "contents": { "en": "The admin has cancelled your " + serviceMode + " booking for the booking id : " + bookingID },
                "include_player_ids": [data.val()["oneSignal"].playerId]
              }
              var body = JSON.stringify(payLoad);
              vm.fetcher.sendNotification(payLoad).then(resp => {
              });
            }
          });

        }
      });
  }
}
