import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { AngularFireDatabase } from 'angularfire2/database';
import { NgxSpinnerService } from 'ngx-spinner';
// import { Observable } from 'rxjs';
// import * as moment from 'moment';
// declare var bootbox: any;
// import * as moment from 'moment-timezone';
// declare var jQuery: any;
// import DataTable from 'datatables.net';


@Component({
  selector: 'app-gro-logged-details',
  templateUrl: './gro-logged-details.component.html',
  styleUrls: ['./gro-logged-details.component.css']
})
export class GroLoggedDetailsComponent implements OnInit {
  dataTable: any;
  staffsPath: any;
  groObj: any;
  groName: any;
  loggedInfo: any;
  constructor(
    private spinner: NgxSpinnerService,
    public db: AngularFireDatabase,
    public chRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.getStaffs();
  }

  getStaffs() {
    this.spinner.show();
    this.staffsPath = this.db.object('staffDetails/').valueChanges();
    this.staffsPath.subscribe(res => {
      this.spinner.hide();
      // this.adminObj = Object.entries(res["admin"]).map(([objId, value]) => ({ objId, value }));
      // this.grlObj = Object.entries(res["grl"]).map(([objId, value]) => ({ objId, value }));
      this.groObj = Object.entries(res["gro"]).map(([objId, value]) => ({ objId, value }));
      this.chRef.detectChanges();
      // const table_1: any = $('#table_1');
      // this.dataTable_1 = table_1.DataTable();
      // const table_2: any = $('#table_2');
      // this.dataTable_2 = table_2.DataTable();
      const table: any = $('#table');
      this.dataTable = table.DataTable();
    })
  }
  getLoggedInfo(groName, loggedInfo) {
    this.groName = groName;
    this.loggedInfo = Object.entries(loggedInfo).map(([objId, value]) => ({ objId, value }));;
    // console.log(this.loggedInfo)
  }
  getMobile(date,item) {
    let mobileNo = 0;
    // console.log(date);
    // console.log(item);
    let lastReg = Object.entries(item).map(([objId, value]) => ({ objId, value }));
    // console.log(lastReg);
    if(lastReg)
    {
      let lastObj = lastReg.slice(-1).pop();
      mobileNo = lastObj.value["mobileNo"];
      // console.log(lastObj.value["mobileNo"]);
    }
    return mobileNo;
  }

}
