import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroLoggedDetailsComponent } from './gro-logged-details.component';

describe('GroLoggedDetailsComponent', () => {
  let component: GroLoggedDetailsComponent;
  let fixture: ComponentFixture<GroLoggedDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroLoggedDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroLoggedDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
