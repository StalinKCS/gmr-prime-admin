import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { AngularFireDatabase } from 'angularfire2/database';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment-timezone';
declare var jQuery: any;
declare var bootbox: any;
import DataTable from 'datatables.net';
import * as FileSaver from 'file-saver';
// declare var $: any;
// declare var bootbox: any;
@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css']
})
export class PaymentsComponent implements OnInit {
  dataTable: any;
  paymentsPath: any;
  paymentsObj: any;
  userPath: any;
  userObj: any;
  constructor(private spinner: NgxSpinnerService, public db: AngularFireDatabase, public chRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.getPaymentsDetails();
    jQuery("#a_from_date").datepicker({ 
      dateFormat: 'dd/mm/yy',
      onSelect: function(start_date){
        jQuery("#a_to_date").datepicker({ 
          dateFormat: 'dd/mm/yy',
        });
        jQuery("#a_to_date").datepicker( "option", "minDate", start_date);
      } 
    });

    jQuery("#d_from_date").datepicker({ 
      dateFormat: 'dd/mm/yy',
      onSelect: function(start_date){
        jQuery("#d_to_date").datepicker({ 
          dateFormat: 'dd/mm/yy',
        });
        jQuery("#d_to_date").datepicker( "option", "minDate", start_date);
      } 
    });

    jQuery("#t_from_date").datepicker({ 
      dateFormat: 'dd/mm/yy',
      onSelect: function(start_date){
        jQuery("#t_to_date").datepicker({ 
          dateFormat: 'dd/mm/yy',
        });
        jQuery("#t_to_date").datepicker( "option", "minDate", start_date);
      } 
    });
  }

  filterTable(table, from, to, col){
    if(jQuery(from).val() && jQuery(to).val()){
      DataTable.ext.search.pop();
      DataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var dateStart = jQuery(from).val();
            var dateEnd = jQuery(to).val();
            var initialDate = data[col].split('at')[0];
            // console.log(slotDate);
            var paymentInitialate = new Date(moment(new Date(initialDate), 'DD/MM/YYYY').format("LLLL"));
            var start = new Date(moment(dateStart, 'DD/MM/YYYY').format("LLLL"));
            var end = new Date(moment(dateEnd, 'DD/MM/YYYY').format("LLLL"));
            if((paymentInitialate >= start) && (paymentInitialate <= end)){
              return true;
            }
            return false;
        }
      );
      var table: any = $(table);
      table = table.DataTable();
      table.draw();
    }else{
      bootbox.alert("Please select a valid date.");
    }
  }



  getPaymentsDetails() {
    this.spinner.show();
    this.paymentsPath = this.db.object('payments/').valueChanges();
    this.paymentsPath.subscribe(res => {
      this.spinner.hide();
      console.log(res);
      this.paymentsObj = Object.entries(res).map(([objId, value]) => ({ objId, value }));
      console.log(this.paymentsObj);
      this.chRef.detectChanges();
      const table: any = $('#table_show');
      this.dataTable = table.DataTable();
    })
  }
  getUserInfo(uid) {
    this.spinner.show();
    this.userPath = this.db.object('userDetails/' + uid).valueChanges();
    this.userPath.subscribe(res => {
      this.spinner.hide();
      this.userObj = res;
    })
  }
  getDate(value) {
    var formatDate;
    var timeStamp = moment.tz(value, "yyyy:MM:DD HH:mm:ss", "Asia/Kolkata").valueOf();
    var date = new Date(timeStamp + 19800000).toString();
    formatDate = date.substring(0, 4) + "-" + date.substring(4, 6) + "-" + date.substring(6, 8) + "T" + date.substring(8, 10) + ":" + date.substring(10, 12) + ":" + date.substring(12, 14) + ".000Z";
    formatDate = moment.utc(date).format("MMM DD YYYY [at] HH:mm A");
    return formatDate;
  }

  getResp(val) {
    let resp;
    if (val.axisResp) {
      resp = val.axisResp.AMT;
    }
    else {
      resp = "-";
    }
    return resp;
  }

  exportToExcel(e) {
    var blob = new Blob([document.getElementById('paymentDetails').outerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    FileSaver.saveAs(blob, 'Payments-' + new Date().getTime());
    // var a = document.createElement('a');
    // //getting data from our div that contains the HTML table
    // var data_type = 'data:application/vnd.ms-excel';
    // var table_div = document.getElementById('paymentDetails');
    // var table_html = table_div.outerHTML.replace(/ /g, '%20');
    // console.log(table_html)
    // a.href = data_type + ', ' + table_html;
    // //setting the file name
    // a.download = 'Payments-' + new Date().getTime();
    // //triggering the function
    // a.click();
    // //just in case, prevent default behaviour
    // e.preventDefault();
  }
}
