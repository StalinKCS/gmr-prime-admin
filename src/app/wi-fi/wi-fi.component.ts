import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
declare var $: any;
declare var bootbox: any;
@Component({
  selector: 'app-wi-fi',
  templateUrl: './wi-fi.component.html',
  styleUrls: ['./wi-fi.component.css']
})
export class WiFiComponent implements OnInit {
  dataTable: any;
  wifi: any;
  wifiObj: any;
  ssid: any;
  key: any;
  constructor(private spinner: NgxSpinnerService, public db: AngularFireDatabase) { }

  ngOnInit() {
    this.getWiFi();
  }

  getWiFi() {
    this.spinner.show();
    this.wifi = this.db.list('wifi/').valueChanges();
    this.wifi.subscribe(res => {
      this.spinner.hide();
      this.wifiObj = Object.entries(res).map(([objID, value]) => ({ objID, value }));
      console.log(this.wifiObj);
    })
  }

  isEmpty(val) {
    return (val === false || val === '' || val === undefined || val == null || val.length <= 0) ? true : false;
  }

  _openModel(username, password) {
    var vm = this;
    vm.ssid = username;
    vm.key = password;
    $('#editModel').modal('show');
  }

  updateWiFi() {
    var vm = this;
    if (vm.isEmpty(vm.ssid)) {
      alert('Please enter a ssid!');
    } else if (vm.isEmpty(vm.key)) {
      alert('Please enter a key!');
    } else {
      let updatePath = 'wifi/info/';
      this.db.object(updatePath).update(
        {
          "ssID": this.ssid,
          "key": this.key

        });
      $('#editModel').modal('hide');
    }

  }

}
