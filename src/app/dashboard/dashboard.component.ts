import { Component, OnInit, ChangeDetectorRef, ɵConsole } from '@angular/core';
import { DatepickerOptions } from '../../ng-datepicker/component/ng-datepicker.component';
import * as enLocale from 'date-fns/locale/en';
import { AngularFireDatabase} from 'angularfire2/database';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment-timezone';
declare var $: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  date: Date;
  minDate: Date;
  disabledDate: { dt: Date, mode: string };
  gro: Observable<any[]>;
  groValues = [];
  dayValues = [];
  dayInt: number;
  grsuper = [];
  groffi = [];
  custData = [];
  rosterGRL: any;
  rosterGRO: any;
  objectKeys = Object.keys;
  selectedBranch: any;
  options: DatepickerOptions = {
    locale: enLocale
  };
  days: any;
  lengthGro: any;
  selectedName: any;
  grodrop: any;
  staffsPath: any;
  grlObj: any;
  groObj: any;
  adminObj: any;
  roles: any;
  currentMonth: any;
  currentYear: any;

  paymentsPath:any;
  paymentsObj:any;
  userPath:any;
  userObj:any;

  constructor(public db: AngularFireDatabase, private spinner: NgxSpinnerService, public chRef: ChangeDetectorRef) {
    this.date = new Date();
    var date = new Date();
    this.days = [
      { day: "SUN" },
      { day: "MON" },
      { day: "TUE" },
      { day: "WED" },
      { day: "THU" },
      { day: "FRI" },
      { day: "SAT" }
    ];
    this.roles = [
      { role: "grl" },
      { role: "gro" }
    ];
  }



  getStaffs() {
    this.spinner.show();
    this.staffsPath = this.db.object('staffDetails/').valueChanges();
    this.staffsPath.subscribe(res => {
      this.spinner.hide();
      this.grlObj = Object.entries(res["grl"]).map(([objId, value]) => ({ objId, value }));
      this.groObj = Object.entries(res["gro"]).map(([objId, value]) => ({ objId, value }));
      this.adminObj = Object.entries(res["admin"]).map(([objId, value]) => ({ objId, value }));
    })
  }

  ngOnInit() {
    this.getGroname();
    this.getStaffs();
    this.getPaymentsDetails();
    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    this.dayInt = firstDay.getDay() + 1;
  }


  // Payment start

  getPaymentsDetails() {
    this.spinner.show();
    this.paymentsPath = this.db.list('/payments', ref => ref.orderByChild('initiatedAt'.valueOf()).limitToLast(5)).valueChanges();
   // this.paymentsPath = this.db.object('payments/').valueChanges();
    this.paymentsPath.subscribe(res => {
      this.spinner.hide();
      this.paymentsObj = Object.entries(res).map(([objId, value]) => ({ objId, value }));
      console.log(this.paymentsObj.reverse());
    })
  }
  getUserInfo(uid) {
    this.spinner.show();
    this.userPath = this.db.object('userDetails/' + uid).valueChanges();
    this.userPath.subscribe(res => {
      this.spinner.hide();
      this.userObj = res;
    })
  }
  getDate_1(value) {
    var formatDate;
    var timeStamp = moment.tz(value, "yyyy:MM:DD HH:mm:ss", "Asia/Kolkata").valueOf();
    var date = new Date(timeStamp + 19800000).toISOString();
    formatDate = date.substring(0, 4) + "-" + date.substring(4, 6) + "-" + date.substring(6, 8) + "T" + date.substring(8, 10) + ":" + date.substring(10, 12) + ":" + date.substring(12, 14) + ".000Z";
    formatDate = moment.utc(date).format("MMM DD YYYY [at] HH:mm A");
    return formatDate;
  }

  // Payment end

  select(date) {
    var em = this;
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    em.dayInt = firstDay.getDay() + 1;
    this.timoutFunction();

  }
  getGroname() {
    this.spinner.show();
    this.db.list('/roster/022020/').valueChanges().subscribe(res => {
      this.spinner.hide();
      this.rosterGRL = res[0];
      this.rosterGRO = res[1];
    });
    this.onChangeBranch('grl');
  }
  toDetect(uid, role) {
    let todaDate = new Date().getDate();
    if (role == 'grl') {
      if (this.rosterGRL.hasOwnProperty(uid)) {
        return this.rosterGRL[uid].schedule[todaDate - 1].shift;
      }
      else {
        return 'None'
      }
    }
    else {
      if (this.rosterGRO.hasOwnProperty(uid)) {
        return this.rosterGRO[uid].schedule[todaDate - 1].shift;
      }
      else {
        return 'None'
      }
    }
  }

  getDate(timeStamp) {
    var date = moment(timeStamp).format("DD-MM-YYYY")
    return date;
  }

  onChange(name) {
    let groValues = [];
    var queryPath = '/roster/022020/' + this.selectedBranch;
    this.db.list(queryPath).valueChanges().subscribe(res => {
      res.forEach((value, index) => {
        groValues.push(value);
        if (groValues[index].name == name) {
          this.lengthGro = groValues.length;
          this.dayValues = groValues[index].schedule;
          this.timoutFunction();
        }
      });
    });
  }

  onChangeBranch(branch) {
    this.selectedBranch = branch;

    this.groValues = [];
    var queryPath = '/roster/022020/' + branch;
    // console.log(queryPath);
    this.db.list(queryPath).valueChanges().subscribe(res => {
      res.forEach(value => {
        this.groValues.push(value);
      });
      this.selectedName = this.groValues[0].name;
      //console.log(this.selectedName);
      this.lengthGro = this.groValues.length;
      this.dayValues = this.groValues[0].schedule;
      this.timoutFunction();
    });
    //   this.groValues = [];
    // this.groValues.push(this.grogrl[branch]);
    // this.grodrop = this.groValues[0];
    // console.log(this.groValues[0].keys);
    // this.selectedName = this.groValues[0];
    // console.log('Selected Name'+JSON.stringify(this.selectedName));
    // this.lengthGro = this.groValues.length;
    // console.log('Length'+this.lengthGro);
    // console.log(this.selectedName[0])
    // this.dayValues = this.selectedName[0]["schedule"];
    // console.log('Dayvalues'+this.dayValues);
  }



  timoutFunction() {
    setTimeout(() => {


      for (var j = 0; j < this.dayValues.length; j++) {
        let element = document.getElementById("wave" + j);
        let comparetxt = element.innerHTML.trim();
        if (j == 0) {
          if (comparetxt === 'D') {

            element.setAttribute("style", "grid-column-start:" + this.dayInt + ";background:#FFCA83");

          }
          else if (comparetxt === 'O') {
            element.setAttribute("style", "grid-column-start:" + this.dayInt + ";background:#FF6565");
          }
          else if (comparetxt === 'N') {
            element.setAttribute("style", "grid-column-start:" + this.dayInt + ";background:#1E4679");
          }
          else if (comparetxt === 'R') {
            element.setAttribute("style", "grid-column-start:" + this.dayInt + ";background:#E9E9F0;color:#000");
          }


        }
        else {
          if (comparetxt === 'D') {

            element.setAttribute("style", "background:#FFCA83");

          }
          else if (comparetxt === 'O') {
            element.setAttribute("style", "background:#FF6565");
          }
          else if (comparetxt === 'N') {
            element.setAttribute("style", "background:#1E4679");
          }
          else if (comparetxt === 'R') {
            element.setAttribute("style", "background:#E9E9F0;color:#000");
          }
        }
      }
    }, 800);
  }

}
