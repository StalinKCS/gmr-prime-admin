import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(public afAuth: AngularFireAuth, public router: Router) { }

    canActivate(): boolean {
         this.afAuth.authState.subscribe((loggedInUser) => {
            if (loggedInUser == null) {
                this.router.navigate(['login']);
                return false;
            }
        });
        return true;
    }

}