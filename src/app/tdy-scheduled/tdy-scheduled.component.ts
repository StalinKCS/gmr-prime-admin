import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { AngularFireDatabase } from 'angularfire2/database';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment-timezone';
declare var bootbox: any;

@Component({
  selector: 'app-tdy-scheduled',
  templateUrl: './tdy-scheduled.component.html',
  styleUrls: ['./tdy-scheduled.component.css']
})
export class TdyScheduledComponent implements OnInit {
  @ViewChild('closeBtn') closeBtn: ElementRef;
  dataTable: any;
  bookedSlotssObj: any;
  bookingObjPath: any;
  bookingInfo: any;
  // ******
  bookingID: any;
  bookedInfo_1: any;

  othersGro: any;
  staffsPath: any;
  groObj: any;
  selectedGro: any;

  constructor(private spinner: NgxSpinnerService, public db: AngularFireDatabase, public chRef: ChangeDetectorRef) {
    this.selectedGro = '';
  }

  ngOnInit() {
    this.getBookingsSlots();
  }
  getBookingsSlots() {
    this.bookingInfo = [];
    this.spinner.show();
    const today = moment.tz('Asia/Kolkata').format('YYYYMMDD');
    var bookedSlotsPath = this.db.object('bookedSlots/' + today).valueChanges();
    bookedSlotsPath.subscribe(res => {
      this.bookedSlotssObj = Object.entries(res).map(([objId, value]) => ({ objId, value }));
      for (let k = 0; k < this.bookedSlotssObj.length; k++) {
        const service = this.bookedSlotssObj[k].value.service;
        const bookingID = this.bookedSlotssObj[k].value.bookingID;
        const bookingObjPath = this.db.object("/bookingDetails/" + service + "/" + bookingID).valueChanges();
        bookingObjPath.subscribe(val => {
          if (val) {
            val["bookingID"] = bookingID;
            this.bookingInfo.push(val);
          }
        });
      }
    })
    this.spinner.hide();
  }
  _bookedInfo(item) {
    this.bookingID = '';
    this.bookedInfo_1 = {};
    this.bookingID = item.bookingID;
    this.bookedInfo_1 = item;
    console.log(this.bookedInfo_1);
  }
  exportToExcel(e) {

  }
  getDate(timeStamp) {
    var date = moment(timeStamp).format("DD-MM-YYYY")
    return date;
  }
  reAssign(uid) {
    this.othersGro = [];
    this.spinner.show();
    this.staffsPath = this.db.object('staffDetails/gro').valueChanges();
    this.staffsPath.subscribe(res => {
      this.spinner.hide();
      this.groObj = Object.entries(res).map(([objId, value]) => ({ objId, value }));
      for (let i = 0; i < this.groObj.length; i++) {
        if (this.groObj[i].objId != uid) {
          this.othersGro.push(this.groObj[i]);
        }
      }
      // console.log(this.othersGro);
    })
  }
  isEmpty(val) {
    return (val === false || val === '' || val === undefined || val == null || val.length <= 0) ? true : false;
  }

  _toUpdate(service, uId) {
    // console.log(service);
    if (this.isEmpty(uId)) {
      bootbox.alert('Please select a GRO!');
    }
    else {
      var vm = this;
      var selectdGRO = {};
      selectdGRO = this.othersGro.filter(
        res => res.objId === uId);
      bootbox.confirm("Are you sure want to Re assign?",
        function (result) {
          if (result) {
            let updatePath = 'bookingDetails/' + service + '/' + vm.bookingID + '/slotInfo/groInfo';
            //console.log(updatePath);
            var payLoad = {
              "assignDate": new Date().getTime(),
              "email": selectdGRO[0].value.email,
              "mobileNo": selectdGRO[0].value.mobileNo,
              "name": selectdGRO[0].value.name,
              "uid": selectdGRO[0].value.uid
            }
            //console.log(payLoad);
            vm.db.object(updatePath).update(payLoad);
            vm.closeModal();
          }
        });
    }
  }
  private closeModal(): void {
    this.othersGro = [];
    this.selectedGro = '';
    this.closeBtn.nativeElement.click();
    //this.chRef.markForCheck();
  }
  // bookingDetails(service, bookingID, property) {
  //   const bookingObjPath = this.db.object("/bookingDetails/" + service + "/" + bookingID).valueChanges();
  //   bookingObjPath.subscribe(res => {
  //     if (property == 'flightDateTime') // flightDateTime
  //     {
  //       if (service == 'tranfer') {
  //         return res["flInfo"].departureInfo.flTimeLabel1 + " " + res["flInfo"].departureInfo.flTimeLabel2.replace(" hrs (IST)", "");
  //       }
  //       else {
  //         return res["flInfo"].flTimeLabel1 + " " + res["flInfo"].flTimeLabel2.replace(" hrs (IST)", "");
  //       }
  //     }
  //     else if (property == 'slotDateTime')// slotDateTime
  //     {
  //       return res["slotInfo"].slotTimeLabel1 + " " + res["slotInfo"].slotTimeLabel2.replace(" hrs (IST)", "");
  //     }
  //     else {
  //       return res["slotInfo"];
  //     }

  //   })
  // }

}
