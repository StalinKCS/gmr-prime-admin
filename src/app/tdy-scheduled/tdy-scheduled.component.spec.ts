import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TdyScheduledComponent } from './tdy-scheduled.component';

describe('TdyScheduledComponent', () => {
  let component: TdyScheduledComponent;
  let fixture: ComponentFixture<TdyScheduledComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TdyScheduledComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TdyScheduledComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
