import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
declare var bootbox: any;
@Component({
  selector: 'app-terms-and-condition',
  templateUrl: './terms-and-condition.component.html',
  styleUrls: ['./terms-and-condition.component.css']
})
export class TermsAndConditionComponent implements OnInit {
  userPath: any;
  userDetails: any;
  conditionOne = false;
  conditionTwo = false;
  constructor(public afAuth: AngularFireAuth, public db: AngularFireDatabase, private spinner: NgxSpinnerService) { }

  ngOnInit() {
  }

  tNC() {
    var vm = this;
    if (this.conditionOne && this.conditionTwo) {
      bootbox.confirm("Are you sure want to proceed?",
        function (result) {
          if (result) {
            var updatedUserData = {};
            vm.spinner.show();
            vm.userPath = vm.db.object('userDetails').valueChanges();
            vm.userPath.subscribe(res => {
              vm.spinner.hide();
              vm.userDetails = Object.entries(res).map(([objId, value]) => ({ objId, value }));
              for (let k = 0; k < vm.userDetails.length; k++) {
                updatedUserData['userDetails/' + vm.userDetails[k].objId + '/tNc'] = false;
              }
              vm.db.object('/').update(updatedUserData);
            })
          }
        });
    }
    else {
      bootbox.alert("Please accept the terms of service to proceed");
    }
  }
}



