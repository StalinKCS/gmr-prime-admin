import { Component, OnInit, ChangeDetectorRef,ViewChild, ElementRef } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { AngularFireDatabase } from 'angularfire2/database';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment-timezone';
import * as FileSaver from 'file-saver';
@Component({
  selector: 'app-guest-info',
  templateUrl: './guest-info.component.html',
  styleUrls: ['./guest-info.component.css']
})
export class GuestInfoComponent implements OnInit {
  dataTable: any;
  dataTable_1: any;
  guestPath: any;
  guestObj: any;
  guest: any;
  guestBookedInfo: any;
  bookingPath: any;
  bookedInfo: any;
  constructor(private spinner: NgxSpinnerService, public db: AngularFireDatabase, public chRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.guest = [];
    this.getGuestsDetails();
  }
  getGuestsDetails() {
    this.spinner.show();
    this.guestPath = this.db.object('userDetails/').valueChanges();
    this.guestPath.subscribe(res => {
      this.spinner.hide();
      this.guestObj = Object.entries(res).map(([objId, value]) => ({ objId, value }));
      //console.log( this.guestObj);
      for (let k = 0; k < this.guestObj.length; k++) {
        if (this.guestObj[k].value.profile && this.guestObj[k].value.profile.name != 'Nuage Laboratoire') {
          this.guest.push(this.guestObj[k]);
        }
      }
      //console.log(this.guest);
      this.chRef.detectChanges();
      const table: any = $('#table_1');
      this.dataTable = table.DataTable();
    })
  }
  getGuestInfo(bookedInfo) {
    this.guestBookedInfo = [];
    this.guestBookedInfo = Object.entries(bookedInfo).map(([objId, value]) => ({ objId, value }));
    console.log(this.guestBookedInfo);
    // this.chRef.detectChanges();
    // const table_1: any = $('#tbl_bookedInfo');
    // this.dataTable_1 = table_1.DataTable();
     //$('#guestInfo').Model('show');
  }

  _bookedInfo(bookedId, serviceMode) {
    var vm = this;
    vm.bookingPath = vm.db.object('bookingDetails/' + serviceMode + '/' + bookedId).valueChanges();
    vm.bookingPath.subscribe(res => {
      vm.spinner.hide();
      vm.bookedInfo = res;
    })

  } 

  exportToExcel(e) {
    var blob = new Blob([document.getElementById('guestDetails').outerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    FileSaver.saveAs(blob, 'Guest-' + new Date().getTime());
  };

  getDate(timeStamp) {
    var formatDate;
    var date = new Date(timeStamp).toString();
    formatDate = date.substring(0, 4) + "-" + date.substring(4, 6) + "-" + date.substring(6, 8) + "T" + date.substring(8, 10) + ":" + date.substring(10, 12) + ":" + date.substring(12, 14) + ".000Z";
    formatDate = moment.utc(date).format("MMM DD YYYY [at] HH:mm A");
    return formatDate;
  }
}
