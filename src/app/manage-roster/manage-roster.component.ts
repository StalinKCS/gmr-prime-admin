import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { NgxSpinnerService } from 'ngx-spinner';
//import * as $ from 'jquery';
// import 'table2excel';
declare var $: any;
declare var bootbox: any;
// import 'datatables.net';
// import 'datatables.net-bs4';
@Component({
    selector: 'app-manage-roster',
    templateUrl: './manage-roster.component.html',
    styleUrls: ['./manage-roster.component.css']
})
export class ManageRosterComponent implements OnInit {
    dataTable: any;
    years: any;
    months: any;
    dates: any;
    selectedMonth: any;
    selectedYear: any;
    // roster: Observable<any>;
    roster: any;
    rosterGRL: any;
    rosterGRO: any;
    uploading: boolean = false;
    percValue: any;
    role: any;
    roleName: any;
    roleId: any;
    shift: any;
    dateIndex: any;
    // currentMonth:any;
    // currentYear:any;
    constructor(private spinner: NgxSpinnerService, public db: AngularFireDatabase, public chRef: ChangeDetectorRef) {
        this.years = [
            {
                year: '2018',
                value: '2018'
            },
            {
                year: '2019',
                value: '2019'
            },
            {
                year: '2020',
                value: '2020'
            },
            {
                year: '2021',
                value: '2021'
            },
            {
                year: '2022',
                value: '2022'
            },
            {
                year: '2023',
                value: '2023'
            },
            {
                year: '2024',
                value: '2024'
            },
            {
                year: '2025',
                value: '2025'
            },
            {
                year: '2026',
                value: '2026'
            },
            {
                year: '2027',
                value: '2027'
            },
            {
                year: '2028',
                value: '2028'
            }
        ];

        this.months = [
            {
                month: '1',
                value: '0'
            },
            {
                month: '2',
                value: '1'
            },
            {
                month: '3',
                value: '2'
            },
            {
                month: '4',
                value: '3'
            },
            {
                month: '5',
                value: '4'
            },
            {
                month: '6',
                value: '5'
            },
            {
                month: '7',
                value: '6'
            },
            {
                month: '8',
                value: '7'
            },
            {
                month: '9',
                value: '8'
            },
            {
                month: '10',
                value: '9'
            },
            {
                month: '11',
                value: '10'
            },
            {
                month: '12',
                value: '11'
            }
        ];
    }

    ngOnInit() {
        var currentMonth = new Date().getMonth();
        this.selectedYear = this.years[1].value;
        this.selectedMonth = this.months[currentMonth].value;
        // console.log(this.selectedYear);
        // console.log(this.selectedMonth);
        this.getDaysInMonth(this.selectedMonth, this.selectedYear);
        this.getRoster();
        // this.currentMonth = this.getMonthName(new Date().getMonth());
        // this.currentYear = new Date().getFullYear();
        // var dateStr = '11/16/2018'; 
        // var day = this.getDayName(dateStr, "en-US");
    }

    getMonthName(month) {
        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"];
        return monthNames[month + 1];
    }

    getRoster() {
        this.spinner.show();
        this.roster = this.db.object('roster/022020').valueChanges();
        this.roster.subscribe(res => {
            this.spinner.hide();
            this.rosterGRL = Object.entries(res["grl"]).map(([objId, value]) => ({ objId, value }));
            //console.log(this.rosterGRL);
            this.rosterGRO = Object.entries(res["gro"]).map(([objId, value]) => ({ objId, value }));
            // console.log(this.rosterGRO);
        })
    }

    getEventDateFormat = function (date) {
        var startDate = date;
        var a = startDate.split("-");
        var year = a[0];
        var month = a[1];
        var day = a[2].substring(0, 2);
        return day + "-" + month + "-" + year;
    }

    calcShift(day, shift) {
        var totalShift = 0;
        // console.log(this.rosterGRL);
        for (let i = 0; i < this.rosterGRL.length; i++) { //Lead
            // console.log(this.rosterGRL[i].value.schedule[day].shift);
            // console.log(shift);
            if (this.rosterGRL[i].value.schedule[day].shift == shift) //Shift
            {
                totalShift = totalShift + 1;
            }
        }
        for (let i = 0; i < this.rosterGRO.length; i++) { //Officer
            if (this.rosterGRO[i].value.schedule[day].shift == shift)  //Shift
            {
                totalShift = totalShift + 1;
            }
        }
        return totalShift;
    }

    _openModel(position, name, id, duty, scheduleIndex) {
        var vm = this;
        vm.role = position;
        vm.roleName = name;
        vm.roleId = id;
        vm.shift = duty;
        vm.dateIndex = scheduleIndex;
        $('#myModal').modal('show');
    }
    updateShift(duty) {
        var vm = this;
        bootbox.confirm("Are you sure want to proceed?",
            function (result) {
                if (result) {
                    let updatePath = 'roster/022020/' + vm.role + '/' + vm.roleId + '/schedule/' + vm.dateIndex;
                    //console.log(updatePath);
                    vm.db.object(updatePath).update(
                        {
                            "shift": duty
                        });
                    $('#myModal').modal('hide');
                }
            });
    }

    getDaysInMonth(month, year) {
        this.dates = [];
        var vm = this;
        var mos = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
        for (var i = 0; i < 12; i++) {
            let lastDate = new Date(year, i + 1, 0);
            if (i == month) {
                for (let k = 1; k <= lastDate.getDate(); k++) {
                    let month1 = parseInt(month) + 1;
                    if (month1 < 10) {
                        month1 = 0 + month1;
                    }
                    if (k < 10) {
                        k = 0 + k;
                    }
                    let currentMonthDays = month1 + '/' + k + '/' + year;

                    //let currentMonthDays =  k+'-'+mos[month]+'-'+year ;
                    vm.dates.push(currentMonthDays);
                }
            }
        }
       // console.log(vm.dates);
    };


    getDayName(dateStr, locale) {
        var date = new Date(dateStr);
        return date.toLocaleDateString(locale, { weekday: 'long' }).substring(0, 3);
    }

    onChange(e) {
        this.getDaysInMonth(this.selectedMonth, this.selectedYear);
    }

    uploadRoster() {
        bootbox.alert('In Progress');
        // this.uploading = true;

        // for (let z = 1; z <= 100; z++) {
        //     setInterval(() => {
        //         this.percValue = z;
        //     }, 2000);
        // }

    }

    exportToExcel(e) {
        // const Table2Excel = (<any>window).Table2Excel;
        // const table2excel = new Table2Excel({
        //     //exclude: ".noExl",
        //     name: "Roster-December-2018",
        //     filename: "Roster" + new Date().getTime(),
        //     exclude_img: true,
        //     exclude_links: true,
        //     exclude_inputs: true
        // });
        // table2excel.export(document.querySelector('#roster'));
        var a = document.createElement('a');
        //getting data from our div that contains the HTML table
        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('staffDetails');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        console.log(a.href);
        //setting the file name
        a.download = 'Roster-' + new Date().getTime();
        //triggering the function
        a.click();
        //just in case, prevent default behaviour
        e.preventDefault();
    }

}
