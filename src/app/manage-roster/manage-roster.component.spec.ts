import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageRosterComponent } from './manage-roster.component';

describe('ManageRosterComponent', () => {
  let component: ManageRosterComponent;
  let fixture: ComponentFixture<ManageRosterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageRosterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRosterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
