import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { AngularFireDatabase } from 'angularfire2/database';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
// import * as moment from 'moment';
declare var bootbox: any;
import * as moment from 'moment-timezone';
declare var jQuery: any;
import DataTable from 'datatables.net';
import * as FileSaver from 'file-saver';
@Component({
  selector: 'app-staff-details',
  templateUrl: './staff-details.component.html',
  styleUrls: ['./staff-details.component.css']
})
export class StaffDetailsComponent implements OnInit {
  dataTable_1: any;
  dataTable_2: any;
  dataTable_3: any;
  staffsPath: any;
  adminObj: any;
  grlObj: any;
  groObj: any;
  rosterGRL: any;
  rosterGRO: any;
  constructor(private spinner: NgxSpinnerService, public db: AngularFireDatabase, public chRef: ChangeDetectorRef) { }

  // ngAfterViewInit() {
  //   this.getUser();
  // }

  ngOnInit() {
    this.getRoster();
    this.getStaffs();
    jQuery("#a_from_date").datepicker({
      dateFormat: 'dd/mm/yy',
      onSelect: function (start_date) {
        jQuery("#a_to_date").datepicker({
          dateFormat: 'dd/mm/yy',
        });
        jQuery("#a_to_date").datepicker("option", "minDate", start_date);
      }
    });

    jQuery("#d_from_date").datepicker({
      dateFormat: 'dd/mm/yy',
      onSelect: function (start_date) {
        jQuery("#d_to_date").datepicker({
          dateFormat: 'dd/mm/yy',
        });
        jQuery("#d_to_date").datepicker("option", "minDate", start_date);
      }
    });

    jQuery("#t_from_date").datepicker({
      dateFormat: 'dd/mm/yy',
      onSelect: function (start_date) {
        jQuery("#t_to_date").datepicker({
          dateFormat: 'dd/mm/yy',
        });
        jQuery("#t_to_date").datepicker("option", "minDate", start_date);
      }
    });
  }

  // getDate(timeStamp) {
  //   var date = moment(timeStamp).format("DD-MM-YYYY")
  //   return date;
  // }

  exportToExcel_1(e) {
    var blob = new Blob([document.getElementById('adminDetails').outerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    FileSaver.saveAs(blob, 'Admin-' + new Date().getTime());
  }

  exportToExcel_2(e) {
    var blob = new Blob([document.getElementById('grlDetails').outerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    FileSaver.saveAs(blob, 'GRL-' + new Date().getTime());
  }

  exportToExcel_3(e) {
    var blob = new Blob([document.getElementById('groDetails').outerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    FileSaver.saveAs(blob, 'GRO-' + new Date().getTime());
  }

  getStaffs() {
    this.spinner.show();
    this.staffsPath = this.db.object('staffDetails/').valueChanges();
    this.staffsPath.subscribe(res => {
      this.spinner.hide();
      this.adminObj = Object.entries(res["admin"]).map(([objId, value]) => ({ objId, value }));
      this.grlObj = Object.entries(res["grl"]).map(([objId, value]) => ({ objId, value }));
      this.groObj = Object.entries(res["gro"]).map(([objId, value]) => ({ objId, value }));
      // console.log(this.groObj);
      this.chRef.detectChanges();
      const table_1: any = $('#table_1');
      this.dataTable_1 = table_1.DataTable();
      const table_2: any = $('#table_2');
      this.dataTable_2 = table_2.DataTable();
      const table_3: any = $('#table_3');
      this.dataTable_3 = table_3.DataTable();
    })
  }
  getDate(timeStamp) {
    var formatDate;
    var date = new Date(timeStamp).toISOString();
    formatDate = date.substring(0, 4) + "-" + date.substring(4, 6) + "-" + date.substring(6, 8) + "T" + date.substring(8, 10) + ":" + date.substring(10, 12) + ":" + date.substring(12, 14) + ".000Z";
    formatDate = moment.utc(date).format("MMM DD YYYY [at] HH:mm A");
    return formatDate;
  }
  getRoster() {
    this.spinner.show();
    this.db.list('/roster/022020/').valueChanges().subscribe(res => {
      this.spinner.hide();
      this.rosterGRL = res[0];
      this.rosterGRO = res[1];
    });
    // this.onChangeBranch('grl');
  }
  toDetect(uid, role) {
    let todaDate = new Date().getDate();
    if (role == 'grl') {
      if (this.rosterGRL.hasOwnProperty(uid)) {
        return this.rosterGRL[uid].schedule[todaDate - 1].shift;
      }
      else {
        return 'None'
      }
    }
    else {
      if (this.rosterGRO.hasOwnProperty(uid)) {
        return this.rosterGRO[uid].schedule[todaDate - 1].shift;
      }
      else {
        return 'None'
      }
    }
  }

  totalCount(item) {
    let totalCount = 0;
    if (item.bookings) {
      let allocatedBooking = Object.entries(item.bookings).map(([objId, value]) => ({ objId, value }));
      totalCount = allocatedBooking.length;
    }
    return totalCount;
  }

  totalScheduled(item, currentStatus) {
    let totalScheduled = 0;
    if (item.bookings) {
      let allocatedBooking = Object.entries(item.bookings).map(([objId, value]) => ({ objId, value }));
      for (let i = 0; i < allocatedBooking.length; i++) {
        const status = allocatedBooking[i].value["status"];
        if (status == currentStatus) {
          totalScheduled++;
        }
      }
    }
    return totalScheduled;
  }
  totalStarted(item, currentStatus) {
    let totalStarted = 0;
    if (item.bookings) {
      let allocatedBooking_1 = Object.entries(item.bookings).map(([objId, value]) => ({ objId, value }));
      for (let i = 0; i < allocatedBooking_1.length; i++) {
        const status = allocatedBooking_1[i].value["status"];
        if (status == currentStatus) {
          totalStarted++;
        }
      }
    }
    return totalStarted;
  }
  totalComplted(item, currentStatus) {
    let totalComplted = 0;
    if (item.bookings) {
      let allocatedBooking_1 = Object.entries(item.bookings).map(([objId, value]) => ({ objId, value }));
      for (let i = 0; i < allocatedBooking_1.length; i++) {
        const status = allocatedBooking_1[i].value["status"];
        if (status == currentStatus) {
          totalComplted++;
        }
      }
    }
    return totalComplted;
  }

  filterTable(table, from, to, col) {
    if (jQuery(from).val() && jQuery(to).val()) {
      // DataTable.ext.search.pop();
      // DataTable.ext.search.push(
      //   function( settings, data, dataIndex ) {
      //       var dateStart = jQuery(from).val();
      //       var dateEnd = jQuery(to).val();
      //       var initialDate = data[col].split('at')[0];
      //       // console.log(slotDate);
      //       var paymentInitialate = new Date(moment(new Date(initialDate), 'DD/MM/YYYY').format("LLLL"));
      //       var start = new Date(moment(dateStart, 'DD/MM/YYYY').format("LLLL"));
      //       var end = new Date(moment(dateEnd, 'DD/MM/YYYY').format("LLLL"));
      //       if((paymentInitialate >= start) && (paymentInitialate <= end)){
      //         return true;
      //       }
      //       return false;
      //   }
      // );
      // var table: any = $(table);
      // table = table.DataTable();
      // table.draw();
    } else {
      bootbox.alert("Please select a valid date.");
    }
  }

  disableStaff(result) {
    var vm = this;
    bootbox.confirm("Are you sure to disable the staff " + result.name + "?",
      function (res) {
        if (res) {
          let payLoad = {
            isEnable: false
          }
          vm.db.object('staffDetails/' + result.role + '/' + result.uid).update(payLoad);
        }
      });
  }
}
