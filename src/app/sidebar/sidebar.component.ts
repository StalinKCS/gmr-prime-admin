import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router, ActivatedRoute, Params } from '@angular/router';
declare var bootbox: any;
declare var $: any;
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  constructor(public afAuth: AngularFireAuth, private route: ActivatedRoute, private router: Router) { 

  }
  ngOnInit() {
    this.removeClass();
  }
  removeClass()
  {
    // $(".sidebar").removeClass("active");
    // $(".side_main").remmoveClass("active");
    // $(".toggle_menu").removeClass("active");
  }
  logOut() {
    var vm = this;
    bootbox.confirm("Are you sure want to log out?",
      function (result) {
        if (result) {
          vm.afAuth.auth.signOut()
            .then((res) =>
              vm.router.navigateByUrl('/login')
            );
        }
      });
  }
  _toOpenSideMenu()
{
  $(".sidebar").toggleClass("active");
  $(".side_main").toggleClass("active");
  $(".toggle_menu").toggleClass("active");
}
}



