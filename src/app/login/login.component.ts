import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
declare var bootbox: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: any;
  password: any;
  constructor(public afAuth: AngularFireAuth, public db: AngularFireDatabase, private spinner: NgxSpinnerService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    // this.afAuth.authState.subscribe((loggedInUser) => {
    //   // console.log(loggedInUser);
    //   if (loggedInUser == null) {
    //     this.router.navigateByUrl('/login');
    //   } else {
    //     this.router.navigateByUrl('/dashboard');
    //   }
    // });
  }
  isEmpty(val) {
    return (val === false || val === '' || val === undefined || val == null || val.length <= 0) ? true : false;
  }

  doLogin() {
    var vm = this;
    if (vm.isEmpty(vm.email)) {
      bootbox.alert('Please enter a email!');
    } else if (vm.isEmpty(vm.password)) {
      bootbox.alert('Please enter your password!')
    } else {
      vm.spinner.show();
      this.afAuth.auth.signInWithEmailAndPassword(vm.email, vm.password).then(
        (resp) => {
          this._toRedirect(resp.user.uid);
        }).catch(
          (err) => {
            vm.spinner.hide();
            bootbox.alert(err.message);
          });
    }
  }
  _toRedirect(uid) {
    var vm = this;
    var profileRef = firebase.database().ref("staffDetails/admin/" + uid).once('value').then(function (snapshot) {
      if (snapshot.val()) {
        vm.spinner.hide();
        if (snapshot.val().role == 'admin') {
          vm.router.navigateByUrl('/dashboard');
        }
        else {
          bootbox.alert("Sorry,you are not admin");
        }
      }
      else {
        bootbox.alert("Oops,something went to wrong");
      }
    });
  }
}
