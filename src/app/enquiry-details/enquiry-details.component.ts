import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { AngularFireDatabase } from 'angularfire2/database';
import { NgxSpinnerService } from 'ngx-spinner';
declare var jQuery: any;
import DataTable from 'datatables.net';
declare var jQuery: any;
declare var bootbox: any;
import * as FileSaver from 'file-saver';
@Component({
  selector: 'app-enquiry-details',
  templateUrl: './enquiry-details.component.html',
  styleUrls: ['./enquiry-details.component.css']
})
export class EnquiryDetailsComponent implements OnInit {

  dataTable: any;
  enquiryPath: any;
  enquiryObj: any;
  constructor(private spinner: NgxSpinnerService, public db: AngularFireDatabase, public chRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.getEnquiryDetails();
    jQuery("#a_from_date").datepicker({
      dateFormat: 'dd/mm/yy',
      onSelect: function (start_date) {
        jQuery("#a_to_date").datepicker({
          dateFormat: 'dd/mm/yy',
        });
        jQuery("#a_to_date").datepicker("option", "minDate", start_date);
      }
    });

    jQuery("#d_from_date").datepicker({
      dateFormat: 'dd/mm/yy',
      onSelect: function (start_date) {
        jQuery("#d_to_date").datepicker({
          dateFormat: 'dd/mm/yy',
        });
        jQuery("#d_to_date").datepicker("option", "minDate", start_date);
      }
    });

    jQuery("#t_from_date").datepicker({
      dateFormat: 'dd/mm/yy',
      onSelect: function (start_date) {
        jQuery("#t_to_date").datepicker({
          dateFormat: 'dd/mm/yy',
        });
        jQuery("#t_to_date").datepicker("option", "minDate", start_date);
      }
    });
  }

  getEnquiryDetails() {
    this.spinner.show();
    this.enquiryPath = this.db.object('enquiryDetails/').valueChanges();
    this.enquiryPath.subscribe(res => {
      this.spinner.hide();
      this.enquiryObj = Object.entries(res).map(([objId, value]) => ({ objId, value }));
      //console.log(this.enquiryObj);
      this.chRef.detectChanges();
      const table: any = $('#table_show');
      this.dataTable = table.DataTable();
    })
  }

  filterTable(table, from, to, col) {
    if (jQuery(from).val() && jQuery(to).val()) {
      // DataTable.ext.search.pop();
      // DataTable.ext.search.push(
      //   function( settings, data, dataIndex ) {
      //       var dateStart = jQuery(from).val();
      //       var dateEnd = jQuery(to).val();
      //       var initialDate = data[col].split('at')[0];
      //       // console.log(slotDate);
      //       var paymentInitialate = new Date(moment(new Date(initialDate), 'DD/MM/YYYY').format("LLLL"));
      //       var start = new Date(moment(dateStart, 'DD/MM/YYYY').format("LLLL"));
      //       var end = new Date(moment(dateEnd, 'DD/MM/YYYY').format("LLLL"));
      //       if((paymentInitialate >= start) && (paymentInitialate <= end)){
      //         return true;
      //       }
      //       return false;
      //   }
      // );
      // var table: any = $(table);
      // table = table.DataTable();
      // table.draw();
    } else {
      bootbox.alert("Please select a valid date.");
    }
  }

  exportToExcel(e) {
    var blob = new Blob([document.getElementById('enquiryDetails').outerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    FileSaver.saveAs(blob, 'enquiry-details-' + new Date().getTime());
    // var a = document.createElement('a');
    // //getting data from our div that contains the HTML table
    // var data_type = 'data:application/vnd.ms-excel';
    // var table_div = document.getElementById('enquiryDetails');
    // var table_html = table_div.outerHTML.replace(/ /g, '%20');
    // a.href = data_type + ', ' + table_html;
    // //setting the file name
    // a.download = 'enquiry-details-' + new Date().getTime();
    // //triggering the function
    // a.click();
    // //just in case, prevent default behaviour
    // e.preventDefault();
  }

}
