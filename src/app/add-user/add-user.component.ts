import { Component, OnInit } from '@angular/core';
// import { FormGroup, FormBuilder, Validators } from '@angular/forms';
declare var bootbox: any;
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import * as firebase from 'firebase';
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  name: any;
  mobileNo: any;
  email: any;
  password: any;
  role: any;
  editData: any;

  constructor(private spinner: NgxSpinnerService, public afAuth: AngularFireAuth, public db: AngularFireDatabase, private route: ActivatedRoute, private router: Router) {
    this.role = '';
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if(params.uid){
        this.editData = params;
        this.name = params.name;
        this.mobileNo = params.mobileNo;
        this.role = params.role;
        this.email = params.email;
        this.password = '*******';
      }
    });
  }

  isEmpty(val) {
    return (val === false || val === '' || val === undefined || val == null || val.length <= 0) ? true : false;
  }

  onSubmit() {
    var vm = this;
    if (vm.isEmpty(vm.name)) {
      bootbox.alert('Please enter a name!');
    } else if (vm.isEmpty(vm.mobileNo)) {
      bootbox.alert('Please enter your mobile number!!');
    } else if (vm.mobileNo.toString().length != 10) {
      bootbox.alert('Please enter a valid mobile number!');
    } else if (vm.isEmpty(vm.role)) {
      bootbox.alert('Please select a role!')
    } else if (vm.isEmpty(vm.email)) {
      bootbox.alert('Please enter a email-ID!');
    } else if (vm.isEmpty(vm.password)) {
      bootbox.alert('Please enter your password!')
    } else {
      if(!this.editData){
        vm.spinner.show();
        vm.afAuth.auth.createUserWithEmailAndPassword(vm.email, vm.password).then(function (resp) {
            vm._toSave(resp);
        }, function (error) {
          vm.spinner.hide();
          bootbox.alert(error.message);
        });
      }else{
        vm._toUpdate();
      }
    }
  }

  _toSave(resp) {
    var vm = this;
    let payLoad = {};
    if (vm.role == 'admin') {
      payLoad = {
        name: vm.name,
        role: vm.role,
        mobileNo: vm.mobileNo,
        email: vm.email,
        createdAt: new Date().getTime(),
        uid: resp.user.uid,
        isEnable: true,
      }
    }
    else {
      payLoad = {
        name: vm.name,
        role: vm.role,
        mobileNo: vm.mobileNo,
        email: vm.email,
        createdAt: new Date().getTime(),
        uid: resp.user.uid,
        isActivated: false,
        isEnable: true,
      }
    }
    vm.db.object('staffDetails/' + vm.role + '/' + resp.user.uid).set(payLoad);
    // Roster
    if (vm.role == 'gro' || vm.role == 'grl') {
      let payLoad1 = {
        email: vm.email,
        mobNo: vm.mobileNo,
        name: vm.name,
        schedule: [
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          },
          {
            shift: "D",
            slots: false
          }
        ],
        "uid": resp.user.uid
      }
      vm.db.object('roster/022020/' + vm.role + '/' + resp.user.uid).update(payLoad1);
    //Update the role
    vm.db.object('staffDetails/roleInfo/staffs/').update({[resp.user.uid]:vm.role});
    }
    vm.spinner.hide();
    bootbox.alert(vm.role + " has been successfully created", function () { vm.router.navigateByUrl('/manage-users'); });
  }


  _toUpdate(){
    var vm = this;
      let payLoad = {
        name: vm.name,
        mobileNo: vm.mobileNo,
        modifiedBy: vm.editData.uid,
        modifiedAt: new Date().getTime()
      }

      let payLoad1 = {
        name: vm.name,
        mobileNo: vm.mobileNo,
      }
      
    vm.db.object('staffDetails/' + vm.editData.role + '/' + vm.editData.uid).update(payLoad);
    vm.db.object('roster/022020/' + vm.editData.role + '/' + vm.editData.uid).update(payLoad1);
    bootbox.alert(vm.editData.role + " has been successfully updated", function () { vm.router.navigateByUrl('/manage-users'); });
  }

}
