import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
declare var $: any;
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
@Component({
  selector: 'app-packag-details',
  templateUrl: './packag-details.component.html',
  styleUrls: ['./packag-details.component.css']
})

export class PackagDetailsComponent implements OnInit {

  service: Observable<any[]>;
  serviceList: any;
  arrivalDomestic: any;
  arrivalInternational: any;
  arrivalListService = [];
  departureListService = [];
  transferListService = [];
  transferList: any;
  currentAmount: any;
  newAmount: any;
  selectedIndex: any;
  selectedService: any;
  selectedTravel: any;
  selectedPackage: any;
  selectedPlan: any;
  submitEnabled: boolean = true;
  uid: any;

  constructor(public afAuth: AngularFireAuth, private spinner: NgxSpinnerService, public db: AngularFireDatabase) { }

  ngOnInit() {
    this.afAuth.authState.subscribe((loggedInUser) => {
      if (loggedInUser) {
        this.uid = loggedInUser.uid;
      }
    });
    this.getServices();
  }

  getServices() {
    this.spinner.show();
    this.service = this.db.list('serviceDetails/').valueChanges();
    this.service.subscribe(res => {
      this.serviceList = Object.keys(res).map(key => (res[key]));
      this.arrivalListService = ["Receive guests at Arrival", "Personalized Services"];
      this.departureListService = ["Personalized Services", "Porter Service", "Lounge"];
      this.transferListService = ["Receive guests at Arrival", "Personalized Services", "Porter Service", "Lounge"];
      this.spinner.hide();
    });
  }

  isArrivalServiceAvailable(serv, arr) {
    for (let i = 0; i < arr.length; i++) {
      if (serv == arr[i].title) {
        return "Yes";
      }
    }
  }


  isDepartureServiceAvailable(serv, arr) {
    for (let i = 0; i < arr.length; i++) {
      if (serv == arr[i].title) {
        return "Yes";
      }
    }
    return "No";
  }

  setPrice(val) {
    return val.toString();
  }

  isTransferServiceAvailable(serv, arr) {
    for (let i = 0; i < arr.length; i++) {
      if (serv == arr[i].title) {
        return "Yes";
      }
    }
    return "No";
  }

  _toClear() {
    this.selectedPlan = "";
    this.selectedService = "";
    this.selectedTravel = "";
    this.selectedPackage = "";
    this.currentAmount = "";
  }


  editArrivalPackageAmount(index, travel, packge, amount) {
    this._toClear();
    this.selectedIndex = index;
    this.selectedService = 'Arrival';
    this.selectedTravel = travel;
    this.selectedPackage = packge;
    this.currentAmount = amount;
    this.submitEnabled = true;
    $('#myModal').modal('show');
  }


  editDepartPackageAmount(plan, packge, travel, amount) {
    this._toClear();
    this.selectedPlan = plan;
    this.selectedService = 'Departure';
    this.selectedTravel = travel;
    this.selectedPackage = packge;
    this.currentAmount = amount;
    this.submitEnabled = true;
    $('#myModal').modal('show');
  }

  editTransferPackageAmount(plan, packge, amount) {
    this._toClear();
    this.selectedPlan = plan;
    this.selectedService = 'Transfer';
    this.selectedPackage = packge;
    this.currentAmount = amount;
    this.submitEnabled = true;
    $('#myModal').modal('show');
  }

  validatePackage(amount) {
    this.newAmount = amount;
    if (this.newAmount == null || this.newAmount == 0) {
      this.submitEnabled = true;
    } else {
      this.submitEnabled = false;
    }
  }


  updatePackageValue() {
    let postData = {};
    if (this.selectedService == "Arrival") {
      let updatePath = "serviceDetails/" + this.selectedService.toLowerCase() + "/" + this.selectedTravel.toLowerCase() + "/services/" + this.selectedIndex;
      this.db.object(updatePath).update({ "amount": this.newAmount });
      // Maintain history
      postData = {
        "modifyBy": this.uid,
        "modifyDate": new Date().getTime(),
        "oldAmount": this.currentAmount,
        "newAmount": this.newAmount,
        "travel": this.selectedTravel,
        "package": this.selectedPackage
      };
      // Get a key for a new Post.
      var newPostKey = firebase.database().ref().child('posts').push().key;
      this.db.object("serviceHistory/arrival/ " + newPostKey).update(postData);
    } else if (this.selectedService == "Departure") {
      let updatePath = "serviceDetails/" + this.selectedService.toLowerCase() + "/" + this.selectedPlan + "/packages/" + this.selectedPackage.toLowerCase();
      this.db.object(updatePath).update({ [this.selectedTravel.toLowerCase() + "Amt"]: this.newAmount });
      // Maintain history
      postData = {
        "modifyBy": this.uid,
        "modifyDate": new Date().getTime(),
        "oldAmount": this.currentAmount,
        "newAmount": this.newAmount,
        "travel": this.selectedTravel,
        "package": this.selectedPackage,
        "plan": this.selectedPlan
      };
      // Get a key for a new Post.
      var newPostKey = firebase.database().ref().child('posts').push().key;
      this.db.object("serviceHistory/departure/ " + newPostKey).update(postData);
    } else if (this.selectedService == "Transfer") {
      let updatePath = "serviceDetails/" + this.selectedService.toLowerCase() + "/" + this.selectedPlan + "/packages/" + this.selectedPackage.toLowerCase();
      this.db.object(updatePath).update({ ['pkgAmount']: this.newAmount });
      // Maintain history
      postData = {
        "modifyBy": this.uid,
        "modifyDate": new Date().getTime(),
        "oldAmount": this.currentAmount,
        "newAmount": this.newAmount,
        "travel": this.selectedTravel,
        "package": this.selectedPackage,
        "plan": this.selectedPlan
      };
      // Get a key for a new Post.
      var newPostKey = firebase.database().ref().child('posts').push().key;
      this.db.object("serviceHistory/transfer/ " + newPostKey).update(postData);
    }
    this.newAmount = "";
    $('#myModal').modal('hide');
  }
}

