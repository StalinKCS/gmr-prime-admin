import { Component } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public afAuth: AngularFireAuth, public router: Router) { }

  //title = 'app';
  ngOnInit() {
    this.afAuth.authState.subscribe((loggedInUser) => {
      if (loggedInUser == null) {
          this.router.navigate(['login']);
      }
      else{
        this.router.navigate(['dashboard']);
      }
  });
  }
}
