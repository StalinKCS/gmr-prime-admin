// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyBeYI6Xx-rprUpL34qbfvJJwub4nHV4hc8",
    authDomain: "gmr-prime.firebaseapp.com",
    databaseURL: "https://gmr-prime.firebaseio.com",
    projectId: "gmr-prime",
    storageBucket: "gmr-prime.appspot.com",
    messagingSenderId: "869210279658",
    baseUrl : "",
    app_id : "160d50df-cfce-4b81-b30b-4a052c7609c0"
   }
};