# MgAdmin

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.10.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


<del>npm install bootstrap --save</del>

<del>npm install jquery --save</del>

npm install popper.js --save

npm install rateyo

npm install font-awesome

npm install angularfire2@5.0.0-rc.3 --save

npm install firebase@4.7.0 --save

npm install bootbox

npm install ngx-spinner --save

npm install --save @ng-bootstrap/ng-bootstrap

npm i angular-calendar

# When finishing go for Bootstrap 4 (use the strict 4.0.0-beta.2 or you will be in trouble with autoprefixer and other stuffs)
npm install bootstrap@4.0.0-beta.2 --save --save-exact

# Then DataTables core 
npm install datatables.net --save

# Then DataTables Bootstrap 4
npm install datatables.net-bs4 --save

# Finaly jQuery and it's types 
npm install jquery --save
npm install @types/jquery --save-dev

# Refer link
https://medium.com/apprendre-le-web-avec-lior/angular-5-and-jquery-datatables-fd1dd2d81d99